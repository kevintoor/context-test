const { init } = require('@mimik/edge-ms-helper/init-helper');

const swaggerMiddleware = require('../build/test-swagger-mw');
const configMiddleware = require('./configuration/configMiddleware');

mimikModule.exports = init([configMiddleware, swaggerMiddleware]);
