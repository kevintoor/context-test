const config = {
  edgeContext: undefined,
  addEdgeContext(edgeContext) {
    this.edgeContext = edgeContext;
  },
};

module.exports = config;
