const config = require('./config');

const middleware = (req, res, next) => {
  console.log('===> in middleware');

  config.addEdgeContext(req.context);
  next();
};

module.exports = middleware;
