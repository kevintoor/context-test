const config = {
  yo: 'lo',
  modify() {
    this.yo = 'no';
  },
};

module.exports = config;
