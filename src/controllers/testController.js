const response = require('@mimik/edge-ms-helper/response-helper');

const testProcessor = require('../processors/testProcessor');

function runTest(req, res) {
  testProcessor.runTest()
    .then(() => {
      response.sendResult({}, 200, res);
    });
}

module.exports = {
  runTest,
};
